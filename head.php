<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title></title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- Place favicon.ico and apple-touch-icon.png in the root directory -->

        <link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/css/main.css">
        <script src="<?php bloginfo('template_url'); ?>/js/vendor/modernizr-2.6.2.min.js"></script>
    </head>
    <body>
        <!--[if lt IE 7]>
            <p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->

        <!-- Add your site or application content here -->
        
            
        
        <header>
            <div class="inner">
                
                <div class="left">
                    <img src="<?php bloginfo('template_url'); ?>/img/E-Brewer-logo.png" alt="">

                    <div class="burger">
                        <span class="b1"></span>
                        <span class="b2"></span>
                        <span class="b3"></span>
                    </div>
                </div>

                <div class="right">
                    
                    <img class="fb" src="<?php bloginfo('template_url'); ?>/img/fb.png" alt="">
                    <div class="call-us clearfix">
                        <span class="pacifico">Call Us&nbsp;</span><br>
                        <a href="tel:093733620"><span class="yellow">09 373 3620</span></a>
                    </div>
                    <div class="cover"></div>
                    <ul>
                        <li><a href="about">About</a></li>
                        <li><a href="services">Services</a></li>
                        <li class="last"><a href="contact">Contact</a></li>
                        <li class="hide"><img class="fb" src="<?php bloginfo('template_url'); ?>/img/fb.png" alt=""></li>
                    </ul>
                </div>
                
                
            </div>
        </header>