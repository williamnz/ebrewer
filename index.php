<?php 
	include 'head.php' 
?>



    <div id="wrapper">
        
        <section class="home">

            <div class="bg"></div>
            <div class="outer"  data-font-parent-base="1200">
                <div class="inner">
    

                    <div class="need-a-plumber">
                        <h2 class="pacifico" data-font-base="{'fontSize':40, 'min':20}"> 
                            Need a plumber you can trust?<br>
                            Residential or commercial, we've got it covered.
                        </h2>
                        <img src="<?php bloginfo('template_url'); ?>/img/logo.png" alt="">

                        <h2 class="bebas" data-font-base="{'fontSize':30, 'min':18}">
                            <span class="sidelines">
                                <span class="sideline l"></span>
                                    Providing quality plumbing services since 1912
                                <span class="sideline r"></span>
                            </span>
                        </h2>


                        <h2 class="call-us"><span class="pacifico" data-font-base="{'fontSize':36, 'min':20}">Give us a call -</span> <span class="bebas yellow" data-font-base="{'fontSize':48, 'min':26}">09 373 3620</span></h2>
                    </div>


                
                </div>
            </div>
        </section>



        <section class="about clearfix">
            <div class="outer" data-font-parent-base="1200">
                <div class="inner">
                    <?php 
                        $args = array(
                            'sort_order' => 'ASC',
                            'sort_column' => 'post_title',
                            'hierarchical' => 1,
                            'exclude' => '',
                            'include' => '',
                            'meta_key' => '',
                            'meta_value' => '',
                            'authors' => '',
                            'child_of' => 0,
                            'parent' => -1,
                            'exclude_tree' => '',
                            'number' => '',
                            'offset' => 0,
                            'post_type' => 'page',
                            'post_status' => 'publish'
                        ); 

                        $pages = get_pages($args);
                        $aboutId;
                        $about;
                        foreach ($pages as $key => $page) {
                            if ($page->post_name === 'about'){
                                $aboutId = $page->ID; 
                                $about = $page;
                            } 
                        }
                            
                        $meta = $simple_mb->the_meta($aboutId);

                        $items = array();

                        for($i = 1; $i<4 ; $i++){
                            if(isset($meta['item'.$i.'_title'])){
                                $newArray = array();
                                $newArray[0] = $meta['item'.$i.'_title'];
                                $itemc = (isset($meta['item'.$i.'_content']))? $meta['item'.$i.'_content'] : '';
                                $newArray[1] = $itemc;

                                array_push($items, $newArray);
                            }
                        }


                    ?>
                    <div class="content">
                        
                        <h1>
                            <span class="sidelines bebas">
                                <span class="sideline l"></span>
                                About Us
                                <span class="sideline r"></span>
                            </span>
                        </h1>
                            
                        <!-- <div class="main">                        
                            <p>
                                E Brewer &amp; Sons offer a range of services from plumbing emergencies, fitting appliances, bathrooms and kitchens, to full renovations and drainage solutions.
                                Established over 100 years ago, we have built a reputation we're are proud of.  We focus on providing quality plumbing services combined with honesty. 
                            </p>
                            <p>
                                Our qualified, certified and licensed team can assist with all of your plumbing requirements. We offer a prompt and reliable service covering all domestic and commercial plumbing, repairs, maintenance and installations.

                            </p>
                        </div>

                        <h2 class="pacifico yellow">
                            Repairs, installations, maintenance, <br>renovations and certifications - we’ve got it covered!
                        </h2>

                        <div class="main">                        
                            <p>
                                Our team are trained in handling a wide range of plumbing, gas, repair and renovation requests. Whether it’s your business or your home, we have the expertise and professional approach to get the job completed on time and within budget.
                            </p>
                        </div>  --> 
                        <?php echo $about->post_content; ?>

                        <div class="break"></div>
                        
                        <?php 
                            for($i=0; $i < sizeof($items); $i++){
                                $centerClass = ($i === sizeof($items)-1 && $i % 2 === 0)? 'center' : '';
                        ?>
                            
                            <div class="half <?php echo $centerClass; ?>">
                                <h3 class="bebas"><span><?php echo $items[$i][0] ?></span></h3>
                                <p><?php echo $items[$i][1] ?></p>
                            </div>
                        
                        <?php
                            }
                        ?>


                        <div class="main">
                            <img class="emblem" src="<?php bloginfo('template_url'); ?>/img/EMBLEM.PNG" alt="">
                        </div>

                    </div>
                    
                </div>
            </div>
        </section>


        <section class="services clearfix">
            <div class="outer" data-font-parent-base="1200">
                <div class="inner">


                    <div class="content">
                        
                        <h1>
                            <span class="sidelines bebas">
                                <span class="sideline l"></span>
                                Our Services
                                <span class="sideline r"></span>
                            </span>
                        </h1>
                        
                        
                        <?php 
                            $args = array(
                                'sort_order' => 'ASC',
                                'sort_column' => 'post_title',
                                'hierarchical' => 1,
                                'exclude' => '',
                                'include' => '',
                                'meta_key' => '',
                                'meta_value' => '',
                                'authors' => '',
                                'child_of' => 0,
                                'parent' => -1,
                                'exclude_tree' => '',
                                'number' => '',
                                'offset' => 0,
                                'post_type' => 'page',
                                'post_status' => 'publish'
                            ); 
                            $pages = get_pages($args);

                            $servicesNum;

                            foreach ($pages as $key => $page) {
                                if ($page->post_name === 'services') $servicesNum = $page->ID;
                            }
                            
                        ?>
                        
                        
                        
                        <select class="bebas services-nav">
                           
                            <?php

                                $argsB = array(
                                    'sort_order' => 'ASC',
                                    'sort_column' => 'menu_order',
                                    'parent' => $servicesNum
                                ); 
                                $pagesB = get_pages($argsB);
                                $count = 0;

                                foreach ($pagesB as $key => $pageB) {
                                    $class = '';
                                    if($count === 0){
                                        $class = 'class="selected"';
                                    }else if($count === sizeof($pagesB)-1){
                                        $class = 'class="last"';
                                    }
                                    $count++;
                            ?>
                                <option <?php echo $class; ?> value="<?php echo $pageB->post_name.'-service-content'; ?>"><?php echo $pageB->post_title; ?></option>
                            <?php
                                }
                            ?>
                            
                        </select>


                        <table class="bebas services-nav">
                            <tr>
                            <?php

                                $argsB = array(
                                    'sort_order' => 'ASC',
                                    'sort_column' => 'menu_order',
                                    'parent' => $servicesNum
                                ); 
                                $pagesB = get_pages($argsB);
                                $count = 0;

                                foreach ($pagesB as $key => $pageB) {
                                    $class = '';
                                    if($count === 0){
                                        $class = 'class="selected"';
                                    }else if($count === sizeof($pagesB)-1){
                                        $class = 'class="last"';
                                    }
                                    $count++;
                            ?>
                                <td <?php echo $class; ?>><a href="<?php echo $pageB->post_name.'-service-content'; ?>"><span class="<?php echo 'icon-'.$pageB->post_name; ?>"></span> <br>   <?php echo $pageB->post_title; ?></a></td>
                            <?php
                                }
                            ?>
                            </tr>
                        </table>
                        
                        <?php 

                        $count = 0;

                        foreach ($pagesB as $key => $pageB) {
                            global $simple_mb;
                            $meta = $simple_mb->the_meta($pageB->ID);
                            $current = ($count === 0)? 'current' : '';
                            $count++;

                            $top = $pageB->post_content;
                            $left = (isset($meta['left_content']))? $meta['left_content'] : '';
                            $right = (isset($meta['right_content']))? $meta['right_content'] : '';
                            $bottom = (isset($meta['bottom_content']))? $meta['bottom_content'] : '';
                        ?>

                        <div class="sub-content <?php echo $pageB->post_name.'-service-content '.$current; ?>">
                            <div class="top"><?php echo $top; ?></div>

                            <div class="left"><?php echo $left; ?></div>

                            <div class="right"><?php echo $right; ?></div>

                            <div class="bottom"><?php echo $bottom; ?></div>

                        </div>
                         
                        <?php
                            }
                        ?>   

                        <div class="height-holder"></div>
                        
                    
                </div>
            </div>
        </section>



        <section class="contact clearfix">

            <div class="border-top"></div>
            <div class="outer" data-font-parent-base="1200">
                <div class="inner">


                    <div class="content">
                        
                        <h1>
                            <span class="sidelines bebas">
                                <span class="sideline l"></span>
                                Contact Us
                                <span class="sideline r"></span>
                            </span>
                        </h1>

                        <?php 
                            $args = array(
                                'sort_order' => 'ASC',
                                'sort_column' => 'post_title',
                                'hierarchical' => 1,
                                'exclude' => '',
                                'include' => '',
                                'meta_key' => '',
                                'meta_value' => '',
                                'authors' => '',
                                'child_of' => 0,
                                'parent' => -1,
                                'exclude_tree' => '',
                                'number' => '',
                                'offset' => 0,
                                'post_type' => 'page',
                                'post_status' => 'publish'
                            ); 
                            $pages = get_pages($args);

                            $servicesNum;

                            foreach ($pages as $key => $page) {
                                if ($page->post_name === 'contact'){
                                    echo $page->post_content;
                                }
                            }
                            
                        ?>

                        <?php echo do_shortcode('[contact-form-7 id="33" title="Contact form 1"]'); ?>



                    </div>
                </div>
            </div>

            <div class="border border-yellow"></div>
            <div class="border border-light-green"></div>
            <div class="border border-green"></div>
        </section>
    </div>




<?php 
	include 'foot.php' 
?>