jQuery(document).ready(function($) {
    
    var $win = $(window);
    var winW = $win.width();
    var winH = $win.height();
    var $home = $('.home');
    var $services = $('.services');
    var $navLiA = $('header li a');
    var $doc = $(document);
    var $bg = $('.bg');
    var $burger = $('.burger');

    $win.on('resize', function(){
        resize();
    }); 

    resize();

    $win.on('load', function(){
        resize();
    });

    $services.find('.services-nav td a').on('click', servicesClick);
    $services.find('select').on('change', servicesChange);

    $navLiA.on('click', function(e){
        e.preventDefault();
        var target = $(this).attr('href');
        var offset = $('.'+target).offset().top-80;
        $('body, html').animate({scrollTop:offset},1000);
        $('header ul, .cover').removeClass('show');
    });

    $burger.on('click', function(){
        $('header ul, .cover').addClass('show');
    });

    $('.cover').on('click', function(){
        $('header ul, .cover').removeClass('show');
    });

    function resize(){
        winH = $win.height();
        winW = $win.width();

         $home.height(winH);
        

        resizeFonts();
        servicesHeight();
    }


    function resizeFonts(){
        $('[data-font-base]').each(function(){
            var p = $(this).closest('[data-font-parent-base]');
            var w = p.width(); // parents width to adjust font size off
            var bw = parseInt(p.data('font-parent-base')); // parents BASE width
            var fb = $(this).data('font-base'); // font BASE width

            fb = fb.replace(/'/g, '"');

            try {
                fb = $.parseJSON(fb.toString());
            } catch(e){
                console.log(e);
            }

            var newFontSize = fb.fontSize/bw*w;
            var newLineHeight = fb.lineHeight/bw*w;
            var obj = {};

            if(fb.letterSpacing){
                obj['letter-spacing'] = (fb.letterSpacing/bw*w)+'px';
            }
            if(fb.min){
                if(newFontSize < fb.min){
                    newLineHeight = newLineHeight/newFontSize * fb.min;
                    newFontSize = fb.min;
                }
            }
            if(fb.max){
                if(newFontSize > fb.max){
                    newLineHeight = newLineHeight/newFontSize * fb.max;
                    newFontSize = fb.max;
                }
            }

            obj['font-size'] = newFontSize+'px';
            obj['line-height'] = newLineHeight+'px';

            $(this).css(obj);
        });
    }

 
    function servicesClick(e){
        e.preventDefault();
        $services.find('.services-nav td').removeClass('selected');

        $(this).parents('td').addClass('selected');

        $services.find('.sub-content').removeClass('current');

        var target = $(this).attr('href');
        $services.find('.'+target).addClass('current');

    }

    function servicesChange(e){
        e.preventDefault();

        $services.find('.sub-content').removeClass('current');

        var target = $(this).val();
        $services.find('.'+target).addClass('current');

    }


    function servicesHeight(){
        var h = 0;
        $services.find('.sub-content').each(function(){
            var nh = $(this).height();
            if( nh > h) h = nh;
        });

        $services.find('.height-holder').height(h);

    }

    




    if(!Modernizr.touch){

        $win.on('scroll',    function(){
            var topD = $doc.scrollTop()/2;

            $bg.css({
                '-webkit-transform':'translate3d(0,'+topD+'px,0)',
                 '-moz-transform':'translate3d(0,'+topD+'px,0)',
                 '-o-transform':'translate3d(0,'+topD+'px,0)',
                 '-ms-transform':'translate3d(0,'+topD+'px,0)',
                 'transform':'translate3d(0,'+topD+'px,0)'
             });

        });
    }
















});