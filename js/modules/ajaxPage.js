define([], function(){
	return {
		load: function(page, callback){
			$.ajax({
				type: 'GET',
				url: page,
				success:function(data){
					callback(data);
				}	
			})
		}
	}
})