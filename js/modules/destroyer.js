define([], function(){
	return function(){
		this.killList = [];
		this.add = function(el, e){
			this.killList.push({el:el,e:e});
		};
		this.destroy = function(){
			for(var i=0; i<this.killList.length; i++){
				var item = this.killList[i]
				var el = item.el;
				var e = item.e
				el.unbind(e);

				console.log(e+' unbound from '+el);
			}
			this.killList = [];
		};
	}
})