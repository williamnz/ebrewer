define([
	'modules/binder',
	'modules/hasTransforms'
	], function(Binder, Transforms){
	
	return function($el){

		var binder = new Binder();
		var animating = false;
		var transforms = Transforms;


		this.options = {
			speed:6
		};

		this.sliderInterval = null;
		this.timerPause = null;

		this.current = 0;
		this.next = function(){
			var images = this.el.find('.slider-image');
			this.current = (this.current >= images.length-1)? 0 : this.current+=1;
			this.animate(this.current, true);
		};
		this.prev = function(){
			var scope = this;
			var images = scope.el.find('.slider-image');

			this.current = (this.current <= 0)? images.length-1 : this.current-=1;
			this.animate(this.current, false);
		};
		this.animate = function(current, forwards){
			var el = this.el;
			var scope = this;
			animating = true;
			setTimeout(function(){
				animating = false;
			},1200);

			if(forwards){
				if(transforms){
					el.find('.slider-image').removeClass('transition');
					setTimeout(function(){

						el.find('.slider-image').removeClass('prev');

						setTimeout(function(){
							el.find('.slider-image').addClass('transition');
							
							setTimeout(function(){
								el.find('.slider-image.current').addClass('prev').removeClass('current');
								el.find('.slider-image:eq('+current+')').addClass('current');
							},0);

						},10);
					},10);
				}else{
					el.find('.slider-image').not('.current').css({
						'left':'100%'
					}).find('.img').css({
						'left':'-95%'
					});
					setTimeout(function(){
						el.find('.slider-image.current').animate({'left':'-100%'},1000,function(){
							$(this).removeClass('current');
						}).find('.img').animate({'left':'95%'},1000);

						el.find('.slider-image:eq('+current+')').animate({'left':'0%'},1000,function(){
							$(this).addClass('current');
						}).find('.img').animate({'left':'0%'},1000);
					},10)
				}
			}else{
				if(transforms){
					el.find('.slider-image').removeClass('transition');
					setTimeout(function(){

						el.find('.slider-image').removeClass('prev');
						el.find('.slider-image:eq('+current+')').addClass('prev');

						setTimeout(function(){
							el.find('.slider-image').addClass('transition');
							
							setTimeout(function(){
								el.find('.slider-image.current').removeClass('current');
								el.find('.slider-image:eq('+current+')').addClass('current').removeClass('prev');
							},0)
						},10)
					},10);
				}else{
					el.find('.slider-image').not('.current').css({
						'left':'-100%'
					}).find('.img').css({
						'left':'95%'
					});
					setTimeout(function(){
						el.find('.slider-image.current').animate({'left':'100%'},1000,function(){
							$(this).removeClass('current');
						}).find('.img').animate({'left':'-95%'},1000);

						el.find('.slider-image:eq('+current+')').animate({'left':'0%'},1000,function(){
							$(this).addClass('current');
						}).find('.img').animate({'left':'0%'},1000);
					},10)
				}
			}
		}

		this.init = function($el){
			var scope = this;
			this.el = $el;
			var images = scope.el.find('.slider-image');
			images.eq(scope.current).addClass('current');
			scope.setTimerAnim();

			if(!transforms){
				this.el.addClass('noTransform');
			}

			binder.bind(scope.el.find('.arrow'), 'click', function(){
				if(!animating){
					clearInterval(scope.sliderInterval);
					clearTimeout(scope.timerPause);
					scope.timerPause = setTimeout(function(){
						scope.setTimerAnim();
					},5000);
					if($(this).hasClass('arrow-left')){
						scope.prev();
					}else{
						scope.next();
					}
				}
			});
		};

		this.setTimerAnim = function(){
			var scope = this;
			scope.sliderInterval = setInterval(function(){
				scope.next();
			},scope.options.speed*1000);
		}

		this.kill=function(){
			var scope = this;
			clearInterval(scope.sliderInterval);
			binder.destroyer.destroy();
		}


		this.init($el);
	}
})