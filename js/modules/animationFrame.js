define([], function(){
	var queue = [];
	var fn =	window.requestAnimationFrame       ||
        		window.webkitRequestAnimationFrame ||
	         	window.mozRequestAnimationFrame    ||
	          	window.oRequestAnimationFrame      ||
        		window.msRequestAnimationFrame     ||
        		function(/* function */ callback, /* DOMElement */ element){
	                window.setTimeout(callback, 1000 / 10);
           		};
	return {
		stopped:true,
	   	add: function(callback, name){
	   		var theAnim = {
	   			name:name,
	   			callback:callback,
	   			stopped:false
	   		}
	   		queue.push(theAnim);
	   	},
	   	remove: function(name){
	   		for(var i=0; i<queue.length; i++){
	   			if(queue[i].name === name){
	   				queue.splice(i,1);
	   			}
	   		}
	   	},
	    anim: function(){
	    	var scope = this;
	    	if(!this.stopped){
	    		var callback;
	    		for(var i=0; i<queue.length; i++){
	    			if(!queue[i].stopped){
	    				callback = queue[i].callback;
		    			callback();
	    			}
	    		}
	    		fn(function(){
	    			scope.anim();
	    		});
	    	}
	    },
	    start: function(name){
	    	if(typeof name === 'undefined'){
	    		if(this.stopped !== false){
	    			this.stopped = false;
	    			this.anim();
	    		}
	    		
	    	}else{
	    		for(var i=0; i<queue.length; i++){
	    			if(queue[i].name === name){
	    				queue[i].stopped = false;
	    			}
	    		}
	    	}
	    },
	    stop: function(name){
	    	if(typeof name === 'undefined'){	
	    		this.stopped = true;
	    	}else{
	    		for(var i=0; i<queue.length; i++){
	    			if(queue[i].name === name){
	    				queue[i].stopped = true;
	    			}
	    		}
	    	}
	    },
	    clearQueue:function(stop){
	    	queue = [];
	    	if(stop){
	    		this.stopped = true;
	    	}
	    }
	}
})