define([
	'modules/home',
	'modules/ajaxPage'
	], function(home , ajax){
	return {
		init:function(){
			var scope = this;
			$(window).on('pageChanged', function(a,b){
				var pageName = b.page;
				var isAjax = b.ajax;
				scope.initPage(pageName, isAjax);
			})
		},
		initPage:function(page, isAjax){
			var callback = function(){};

			if(page === 'home.php'){
				callback = function(data){
					home.init(data);
				}
			} 




			if(isAjax){
				ajax.load(page, callback);
			}else{
				callback();
			}
		}
	}
})