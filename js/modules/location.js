define([], function(){
	return {
		init: function(){
			var scope = this;
			History.Adapter.bind(window,'statechange',function(){
       			scope.pageChanged(true);
    		});
    		scope.pageChanged(false);
		},

		pageChanged:function(ajax){
			var scope = this;
			scope.state = History.getState();
			var page = scope.pageName();
			console.log(ajax);
			$(window).trigger('pageChanged', {
				page:page,
				url:scope.state.url,
				hash:scope.state.hash,
				title:scope.state.title || page,
				ajax:ajax
			});
		},
		
		pageName: function(){
			var scope = this;
			var spliturl = scope.state.url.split('/');
			var name = spliturl.pop();
			return name;
		},

		changePage: function(page, title){
			title = title || page;
			History.pushState({}, title, page);
		}
	}
})