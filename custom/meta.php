<div class="my_meta_control">
 	<?php 
	 	$parentId = $post->post_parent;
	 	$parent = get_post($parentId);

	 ?>
 	<pre>
 	<?php //print_r($parent); ?>
 	</pre>


	 
 	
	
	<?php if($parent->post_name === 'services'):?>
	
	 	<h2 class="tbTitle">Left</h2>
	    <?php $mb->the_field('left_content'); ?>
	    <div class="customEditor"><textarea name="<?php $mb->the_name(); ?>"><?php echo htmlspecialchars_decode($mb->get_the_value()); ?></textarea></div>
	 	<br><br><h2 class="tbTitle">Right</h2>
	    <?php $mb->the_field('right_content'); ?>
	    <div class="customEditor"><textarea name="<?php $mb->the_name(); ?>"><?php echo htmlspecialchars_decode($mb->get_the_value()); ?></textarea></div>
	 	<br><br><h2 class="tbTitle">Bottom</h2>
	    <?php $mb->the_field('bottom_content');?>
	    <div class="customEditor"><textarea name="<?php $mb->the_name(); ?>"><?php echo htmlspecialchars_decode($mb->get_the_value()); ?></textarea></div>
 	
 	<?php elseif ($post->post_name === 'about'): ?>
 		
		<h2 class="tbTitle">Item 1</h2>
	    <?php $mb->the_field('item1_title'); ?>
	    <div class="title"><textarea name="<?php $mb->the_name(); ?>"><?php echo $mb->get_the_value(); ?></textarea></div>
	    <?php $mb->the_field('item1_content'); ?>
	    <div class=""><textarea name="<?php $mb->the_name(); ?>"><?php echo htmlspecialchars_decode($mb->get_the_value()); ?></textarea></div>


	    
	 	<br><br><h2 class="tbTitle">Item 2</h2>
	 	<?php $mb->the_field('item2_title'); ?>
	    <div class="title"><textarea name="<?php $mb->the_name(); ?>"><?php echo $mb->get_the_value(); ?></textarea></div>
	    <?php $mb->the_field('item2_content'); ?>
	    <div class=""><textarea name="<?php $mb->the_name(); ?>"><?php echo htmlspecialchars_decode($mb->get_the_value()); ?></textarea></div>


	 	<br><br><h2 class="tbTitle">Item 3</h2>
	    <?php $mb->the_field('item3_title'); ?>
	    <div class="title"><textarea name="<?php $mb->the_name(); ?>"><?php echo $mb->get_the_value(); ?></textarea></div>
	    <?php $mb->the_field('item3_content');?>
	    <div class=""><textarea name="<?php $mb->the_name(); ?>"><?php echo htmlspecialchars_decode($mb->get_the_value()); ?></textarea></div>


	    <br><br><h2 class="tbTitle">Item 4</h2>
	    <?php $mb->the_field('item4_title'); ?>
	    <div class="title"><textarea name="<?php $mb->the_name(); ?>"><?php echo $mb->get_the_value(); ?></textarea></div>
	    <?php $mb->the_field('item4_content');?>
	    <div class=""><textarea name="<?php $mb->the_name(); ?>"><?php echo htmlspecialchars_decode($mb->get_the_value()); ?></textarea></div>
 	
 	<?php endif; ?>

</div>