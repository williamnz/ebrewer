<?php


include_once 'metaboxes/setup.php';

include_once 'metaboxes/simple-spec.php';
 
// include_once 'metaboxes/full-spec.php';

// include_once 'metaboxes/checkbox-spec.php';

// include_once 'metaboxes/radio-spec.php';

// include_once 'metaboxes/select-spec.php';


// echo TEMPLATEPATH;


 
// lets use the WPAlchemy helper
include_once WP_CONTENT_DIR . '/wpalchemy/MetaBox.php';


 
// custom constant (opposite of TEMPLATEPATH)
define('_TEMPLATEURL',WP_CONTENT_URL . '/themes/' . basename(TEMPLATEPATH));
 
// custom css for our meta boxes
if (is_admin()) wp_enqueue_style('custom_meta_css',_TEMPLATEURL . '/custom/meta.css');
 
// create the meta box
$custom_metabox = new WPAlchemy_MetaBox(array
(
    'id' => '_custom_meta',
    'title' => 'Page Content',
    'template' => TEMPLATEPATH . '/custom/meta.php'
));
 
// important: note the priority of 99, the js needs to be placed after tinymce loads
add_action('admin_print_footer_scripts','my_admin_print_footer_scripts',99);
function my_admin_print_footer_scripts()
{
    ?><script type="text/javascript">/* <![CDATA[ */
        jQuery(function($)
        {
            var i=1;
            $('.customEditor textarea').each(function(e)
            {
                var id = $(this).attr('id');
 
                if (!id)
                {
                    id = 'customEditor-' + i++;
                    $(this).attr('id',id);
                }
 
                tinyMCE.execCommand('mceAddControl', false, id);
                 
            });
        });
    /* ]]> */</script><?php
}

?>